package dk.semlerit.autocore.ws.racf200901.client;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import dk.semlerit.autocore.exception.ServiceClientException;
import dk.semlerit.autocore.web.annotation.CacheQualifier;
import dk.semlerit.autocore.web.annotation.CacheScope;
import dk.semlerit.autocore.web.cache.Cache;
import dk.semlerit.autocore.web.cache.Element;
import dk.semlerit.autocore.ws.annotation.BaseURI;
import dk.semlerit.autocore.ws.annotation.ConsumerId;
import dk.semlerit.autocore.ws.client.WSClientSupport;
import dk.semlerit.autocore.ws.racf200901.client.data.RetrieveDealerNumbersRequest;
import dk.semlerit.autocore.ws.racf200901.client.data.RetrieveDealerNumbersResponse;
import lombok.NonNull;

/**
 * Racf Service implementation.
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@ApplicationScoped
public class RacfService200901ClientImpl implements RacfService200901Client {
	private static final String CACHEKEY_DEALERNUMBERS_PREFIX = "ac.dealernumbers.key";
	private static final String ENDPOINT = "xs/200901/Racf";

	@Inject
	@BaseURI
	private String baseURI;
	@Inject
	@ConsumerId
	private String consumerId;
	@Inject
	@CacheQualifier(CacheScope.SERVER)
	private Cache serverCache;

	private WSClientSupport wsClientSupport;

	@Override
	public List<String> retrieveDealerNumbers(String racfId) {
		return retrieveDealerNumbers(racfId, true);
	}

	@Override
	public List<String> retrieveDealerNumbers(String racfId, boolean useCache) {
		final String cacheKey = getCacheKey(racfId);
		if (useCache) {
			final Element element = serverCache.get(cacheKey);
			if (Objects.nonNull(element)) {
				final List<String> dealerNumbers = (List<String>) element.getValue();
				if (dealerNumbers != null && !dealerNumbers.isEmpty()) {
					return element.getValue();
				}
			}
		}

		final RetrieveDealerNumbersRequest requestPayload = new RetrieveDealerNumbersRequest()
				.withRequest(new RetrieveDealerNumbersRequest.Request().withRacfid(racfId));
		final RetrieveDealerNumbersResponse responseWrapper = (RetrieveDealerNumbersResponse) wsClientSupport
				.marshalSendAndReceive(requestPayload);
		final RetrieveDealerNumbersResponse.Response response = Objects.requireNonNull(responseWrapper.getResponse(),
				"No response was found in reply from ESB");

		final List<String> dealerNumbers = response.getDealernumbers();

		boolean isDealerNumberAvailable = dealerNumbers != null && !dealerNumbers.isEmpty();
		if (useCache && isDealerNumberAvailable) {
			serverCache.put(cacheKey, dealerNumbers, CACHEKEY_DEALERNUMBERS_PREFIX);
		}
		return dealerNumbers;
	}

	@Override
	public void resetCache() throws ServiceClientException {
		serverCache.evict(CACHEKEY_DEALERNUMBERS_PREFIX);
	}

	@Override
	public void resetCache(final String racfId) throws ServiceClientException {
		serverCache.evict(getCacheKey(racfId));
	}

	// ~ Internal methods =====================================================

	private String getCacheKey(@NonNull String racfId) {

		return String.join(".", CACHEKEY_DEALERNUMBERS_PREFIX, racfId);
	}

	// ~ CDI Methods ==========================================================

	@PostConstruct
	@Inject
	public void init() {

		this.wsClientSupport = new WSClientSupport(consumerId,
				URI.create(String.join("/", String.valueOf(baseURI), ENDPOINT)), RetrieveDealerNumbersResponse.class);
	}
}
