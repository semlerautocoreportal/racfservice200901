package dk.semlerit.autocore.ws.racf200901.client;

import java.util.List;

import dk.semlerit.autocore.exception.ServiceClientException;

/**
 * RACF Service 200901 Client
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public interface RacfService200901Client {

	/**
	 * Default retrieval using cached values if available.
	 * 
	 * @param racfId
	 * @return a list of dealer numbers or an empty list
	 * @throws ServiceClientException
	 */

	List<String> retrieveDealerNumbers(String racfId) throws ServiceClientException;

	/**
	 * Retrieval of dealerNumbers, with optional cache usage
	 * 
	 * @param racfId
	 * @param useCache
	 * @return a list of dealer numbers or an empty list.
	 * @throws ServiceClientException
	 */
	List<String> retrieveDealerNumbers(final String racfId, boolean useCache) throws ServiceClientException;

	/**
	 * Reset all cached entries disregarding users.
	 * 
	 * @throws ServiceClientException
	 */
	void resetCache() throws ServiceClientException;

	/**
	 * Reset cached dealerNumbers for a specific user
	 * 
	 * @throws ServiceClientException
	 */
	void resetCache(String racfId) throws ServiceClientException;

}
